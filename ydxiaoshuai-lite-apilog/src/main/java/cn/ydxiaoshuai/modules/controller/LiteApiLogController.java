package cn.ydxiaoshuai.modules.controller;

import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.ydxiaoshuai.common.api.vo.Result;
import cn.ydxiaoshuai.common.aspect.annotation.AutoLog;
import cn.ydxiaoshuai.common.system.base.controller.JeecgController;
import cn.ydxiaoshuai.common.system.query.QueryGenerator;
import cn.ydxiaoshuai.modules.entity.LiteApiLog;
import cn.ydxiaoshuai.modules.service.ILiteApiLogService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

 /**
 * @Description: API日志记录表
 * @Author: 小帅丶
 * @Date:   2020-04-30
 * @Version: V1.0
 */
@Slf4j
@Api(tags="API日志记录表")
@RestController
@RequestMapping("/log/liteApiLog")
public class LiteApiLogController extends JeecgController<LiteApiLog, ILiteApiLogService> {
	@Autowired
	private ILiteApiLogService liteApiLogService;
	
	/**
	 * 分页列表查询
	 *
	 * @param liteApiLog
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "API日志记录表-分页列表查询")
	@ApiOperation(value="API日志记录表-分页列表查询", notes="API日志记录表-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(LiteApiLog liteApiLog,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<LiteApiLog> queryWrapper = QueryGenerator.initQueryWrapper(liteApiLog, req.getParameterMap());
		Page<LiteApiLog> page = new Page<LiteApiLog>(pageNo, pageSize);
		IPage<LiteApiLog> pageList = liteApiLogService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 * 添加
	 *
	 * @param liteApiLog
	 * @return
	 */
	@AutoLog(value = "API日志记录表-添加")
	@ApiOperation(value="API日志记录表-添加", notes="API日志记录表-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody LiteApiLog liteApiLog) {
		liteApiLogService.save(liteApiLog);
		return Result.ok("添加成功！");
	}
	
	/**
	 * 编辑
	 *
	 * @param liteApiLog
	 * @return
	 */
	@AutoLog(value = "API日志记录表-编辑")
	@ApiOperation(value="API日志记录表-编辑", notes="API日志记录表-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody LiteApiLog liteApiLog) {
		liteApiLogService.updateById(liteApiLog);
		return Result.ok("编辑成功!");
	}
	
	/**
	 * 通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "API日志记录表-通过id删除")
	@ApiOperation(value="API日志记录表-通过id删除", notes="API日志记录表-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		liteApiLogService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 * 批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "API日志记录表-批量删除")
	@ApiOperation(value="API日志记录表-批量删除", notes="API日志记录表-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.liteApiLogService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功！");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "API日志记录表-通过id查询")
	@ApiOperation(value="API日志记录表-通过id查询", notes="API日志记录表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		LiteApiLog liteApiLog = liteApiLogService.getById(id);
		return Result.ok(liteApiLog);
	}

  /**
   * 导出excel
   *
   * @param request
   * @param liteApiLog
   */
  @RequestMapping(value = "/exportXls")
  public ModelAndView exportXls(HttpServletRequest request, LiteApiLog liteApiLog) {
      return super.exportXls(request, liteApiLog, LiteApiLog.class, "API日志记录表");
  }

  /**
   * 通过excel导入数据
   *
   * @param request
   * @param response
   * @return
   */
  @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
  public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
      return super.importExcel(request, response, LiteApiLog.class);
  }

}
