package cn.ydxiaoshuai.common.constant;

/**
 * @author 小帅丶
 * @className ImageClassifyNoneName
 * @Description 无数据中文值
 * @Date 2020/9/24-15:40
 **/
public class ImageClassifyNoneName {
    public static String CAR = "非车类";
    public static String ANIMAL = "非动物";
    public static String PLANT = "非植物";
    public static String INGREDIENT = "非果蔬食材";
    public static String FLOWER = "非花";
    public static String DISH = "非菜";
}
