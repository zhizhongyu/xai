package cn.ydxiaoshuai.common.constant;

/**
 * @author 小帅丶
 * @className ImageClassifyConstant
 * @Description 接口类型
 * @Date 2020/9/24-13:50
 **/
public enum ImageClassifyApiType {
    animal,dish,plant,ingredient,car,logo,flower,landmark,redwine,driverbehavior,currency,colourize,color_enhance,image_definition_enhance,selfie_anime,style_trans;
}
