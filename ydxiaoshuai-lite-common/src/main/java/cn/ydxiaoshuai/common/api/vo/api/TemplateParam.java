package cn.ydxiaoshuai.common.api.vo.api;

import lombok.Data;

/**
 * @author 小帅丶
 * @className TemplateBean
 * @Description 模板图信息，要求被融合的人脸
 * @Date 2020/5/29-14:17
 **/
@Data
public class TemplateParam extends MergeBaseBean{
}
