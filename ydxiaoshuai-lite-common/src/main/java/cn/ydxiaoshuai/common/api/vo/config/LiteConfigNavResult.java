package cn.ydxiaoshuai.common.api.vo.config;

import cn.ydxiaoshuai.common.api.vo.BaseResponseBean;
import cn.ydxiaoshuai.common.constant.CommonConstant;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

/**
 * @author 小帅丶
 * @className IsmIndexNavResult
 * @Description 首页导航对象
 * @Date 2020/3/25-11:52
 **/
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LiteConfigNavResult extends BaseResponseBean {
    /**
     * 具体参数
     **/
    private List<NavListData> data;
    @Data
    public static class NavListData{
        //导航名称
        private String nav_name;
        //导航拼音|单词
        private String nav_letter;
        //导航拼音|单词
        private String icon_name;
        //导航图标URL
        private String nav_img_url;
        //导航web跳转地址
        private String nav_web_url;
        //导航小程序跳转路由
        private String nav_lite_url;
        //排序ID
        private Integer nav_sort;
    }
    public LiteConfigNavResult success(String message, List<NavListData> data) {
        this.message = message;
        this.code = CommonConstant.SC_OK_200;
        this.data = data;
        return this;
    }
    public LiteConfigNavResult fail(String message, Integer code) {
        this.message = message;
        this.code = code;
        return this;
    }
    public LiteConfigNavResult error(String message) {
        this.message = message;
        this.code = CommonConstant.SC_INTERNAL_SERVER_ERROR_500;
        return this;
    }
}
