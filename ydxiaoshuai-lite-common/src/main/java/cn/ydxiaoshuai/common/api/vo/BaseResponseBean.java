package cn.ydxiaoshuai.common.api.vo;

import cn.hutool.core.util.IdUtil;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;

/**
 * @Description 基础类
 * @author 小帅丶
 * @className BaseResponseBean
 * @Date 2019/10/21-15:42
 **/
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BaseResponseBean implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final long timestamps = System.currentTimeMillis();
    /**
     * 返回处理消息
     */
    public String log_id = timestamps+"-"+IdUtil.fastSimpleUUID();
    /**
     * 返回处理消息
     */
    public String message = "ok";
    /**
     * 返回处理消息
     */
    public String message_zh = "操作成功！";
    /**
     * 返回代码
     */
    public Integer code = 200;

    /**
     * 时间戳
     */
    public long timestamp = timestamps;

    /**
     * 作者
     */
    public String author = "小帅丶";

}
