package cn.ydxiaoshuai.common.api.vo.api;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author 小帅丶
 * @className MedicalBeautyEyesAttrResponseBean
 * @Description 黑眼圈接口返回的对象
 * @Date 2020/8/26-17:38
 **/
@NoArgsConstructor
@Data
public class MedicalBeautyEyesAttrResponseBean {


    private int error_code;
    private String error_msg;
    private long log_id;
    private int timestamp;
    private int cached;
    private ResultBean result;

    @NoArgsConstructor
    @Data
    public static class ResultBean {

        private int face_num; //图片中的人脸数量
        private List<FaceListBean> face_list;//人脸信息列表

        @NoArgsConstructor
        @Data
        public static class FaceListBean {

            private String face_token;//人脸标志
            private LocationBean location;//人脸在图片中的位置
            private EyesattrBean eyesattr;//眼睛属性信息

            @NoArgsConstructor
            @Data
            public static class LocationBean {
                private double left;//人脸区域离左边界的距离
                private double top;//人脸区域离上边界的距离
                private int width;//人脸区域的宽度
                private int height;//人脸区域的高度
                private int degree;//人脸框相对于竖直方向的顺时针旋转角，[-180,180]
            }

            @NoArgsConstructor
            @Data
            public static class EyesattrBean {
                private List<List<DarkCircleLeftBean>> dark_circle_left;//左眼黑眼圈
                private List<List<DarkCircleRightBean>> dark_circle_right;//右眼黑眼圈
                private List<List<EyeBagsLeftBean>> eye_bags_left;//左眼眼袋
                private List<List<EyeBagsRightBean>> eye_bags_right;//右眼眼袋

                @NoArgsConstructor
                @Data
                public static class DarkCircleLeftBean {

                    private int x;//黑眼圈离左边界的距离
                    private int y;//黑眼圈离上边界的距离
                }

                @NoArgsConstructor
                @Data
                public static class DarkCircleRightBean {

                    private int x;//黑眼圈离左边界的距离
                    private int y;//黑眼圈离上边界的距离
                }
                @NoArgsConstructor
                @Data
                public static class EyeBagsLeftBean {

                    private int x;//眼袋离左边界的距离
                    private int y;//眼袋离上边界的距离
                }
                @NoArgsConstructor
                @Data
                public static class EyeBagsRightBean {

                    private int x;//眼袋离左边界的距离
                    private int y;//眼袋离上边界的距离
                }
            }
        }
    }
}
