package cn.ydxiaoshuai.common.api.vo.faceorgans;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 小帅丶
 * @className FaceMessageBean
 * @Description 每日赠言
 * @Date 2020/9/21-14:19
 **/
@NoArgsConstructor
@Data
public class FaceMessageBean {

    /**
     * extData : {"template_name":"atom/quoteToday","template_id":"G698"}
     * commonData : {"feRoot":"https://mms-static.cdn.bcebos.com/graph/graphfe/static","os":"ios","sidList":"10007_10802_10915_10913_11006_10921_10905_10015_10901_10942_10907_11013_10962_10971_10968_10974_11032_11123_13007_12201_13203_16109_16201_17001_9999","graphEnv":"wise","nativeSdk":false,"sf":0,"isNaView":0,"isHalfWap":0}
     * tplData : {"faceLevel":{"imgUrl":"https://mms-graph.cdn.bcebos.com/face/todayadvice/195.jpg","desc":{"quote":"人生有许多事情，正如船后的波纹，总要过后才觉得美的。","author":"余光中"}},"faceFortune":{"imgUrl":"https://mms-graph.cdn.bcebos.com/face/todayadvice/79.jpg","desc":{"quote":"世界上最宽阔的是海洋，比海洋更宽阔的是天空，比天空更宽阔的是人的胸怀。","author":"雨果"}},"default":"faceFortune"}
     */

    private ExtDataBean extData;
    private CommonDataBean commonData;
    private TplDataBean tplData;

    @NoArgsConstructor
    @Data
    public static class ExtDataBean {
        /**
         * template_name : atom/quoteToday
         * template_id : G698
         */

        private String template_name;
        private String template_id;
    }

    @NoArgsConstructor
    @Data
    public static class CommonDataBean {
        /**
         * feRoot : https://mms-static.cdn.bcebos.com/graph/graphfe/static
         * os : ios
         * sidList : 10007_10802_10915_10913_11006_10921_10905_10015_10901_10942_10907_11013_10962_10971_10968_10974_11032_11123_13007_12201_13203_16109_16201_17001_9999
         * graphEnv : wise
         * nativeSdk : false
         * sf : 0
         * isNaView : 0
         * isHalfWap : 0
         */

        private String feRoot;
        private String os;
        private String sidList;
        private String graphEnv;
        private boolean nativeSdk;
        private int sf;
        private int isNaView;
        private int isHalfWap;
    }

    @NoArgsConstructor
    @Data
    public static class TplDataBean {
        /**
         * faceLevel : {"imgUrl":"https://mms-graph.cdn.bcebos.com/face/todayadvice/195.jpg","desc":{"quote":"人生有许多事情，正如船后的波纹，总要过后才觉得美的。","author":"余光中"}}
         * faceFortune : {"imgUrl":"https://mms-graph.cdn.bcebos.com/face/todayadvice/79.jpg","desc":{"quote":"世界上最宽阔的是海洋，比海洋更宽阔的是天空，比天空更宽阔的是人的胸怀。","author":"雨果"}}
         * default : faceFortune
         */

        private FaceLevelBean faceLevel;
        private FaceFortuneBean faceFortune;
        private String defaultX;

        @NoArgsConstructor
        @Data
        public static class FaceLevelBean {
            /**
             * imgUrl : https://mms-graph.cdn.bcebos.com/face/todayadvice/195.jpg
             * desc : {"quote":"人生有许多事情，正如船后的波纹，总要过后才觉得美的。","author":"余光中"}
             */

            private String imgUrl;
            private DescBean desc;

            @NoArgsConstructor
            @Data
            public static class DescBean {
                /**
                 * quote : 人生有许多事情，正如船后的波纹，总要过后才觉得美的。
                 * author : 余光中
                 */

                private String quote;
                private String author;
            }
        }

        @NoArgsConstructor
        @Data
        public static class FaceFortuneBean {
            /**
             * imgUrl : https://mms-graph.cdn.bcebos.com/face/todayadvice/79.jpg
             * desc : {"quote":"世界上最宽阔的是海洋，比海洋更宽阔的是天空，比天空更宽阔的是人的胸怀。","author":"雨果"}
             */

            private String imgUrl;
            private DescBeanX desc;

            @NoArgsConstructor
            @Data
            public static class DescBeanX {
                /**
                 * quote : 世界上最宽阔的是海洋，比海洋更宽阔的是天空，比天空更宽阔的是人的胸怀。
                 * author : 雨果
                 */

                private String quote;
                private String author;
            }
        }
    }
}
