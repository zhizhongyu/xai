package cn.ydxiaoshuai.modules.liteuser.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import cn.ydxiaoshuai.modules.liteuser.entity.LiteUserInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 微信用户信息表
 * @Author: 小帅丶
 * @Date:   2020-05-14
 * @Version: V1.0
 */
public interface LiteUserInfoMapper extends BaseMapper<LiteUserInfo> {

}
