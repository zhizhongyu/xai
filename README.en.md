[![作者](https://img.shields.io/badge/%E4%BD%9C%E8%80%85-%E5%B0%8F%E5%B8%85%E4%B8%B6-7AD6FD.svg)](https://www.ydxiaoshuai.cn/)

# XAIBoot-separate

#### 介绍
小帅一点资讯新版后端代码使用Springboot+ANTD Vue

#### 软件架构
基于JeecgBoot项目开发

#### 前端项目地址

https://gitee.com/xshuai/xaiboot-vue

#### 小程序端项目地址

https://gitee.com/xshuai/weixinxiaochengxu


#### 使用说明

1.  创建数据库xaiboot_lite，并导入xai\sql\xaiboot_lite.sql数据库文件
2.  修改application-test.yml配置文件中mysql环境内容为自己本地环境的IP、端口、用户名、密码以及项目的端口和项目访问名称
3.  运行LiteApplication.java
4.  登录后台的用户名为admin 密码为 123456
5.  修改xai\ydxiaoshuai-lite-common\src\main\java\cn\ydxiaoshuai\common\constant\GarbageConts.java(APPKEY、SECRETKEY)为垃圾检测接口(请去京东AI平台创建应用)
6.  修改xai\ydxiaoshuai-lite-common\src\main\java\cn\ydxiaoshuai\common\factory\BDFactory.java 相关应用的值(请去百度AI平台创建应用)，绝大数接口都使用百度AI
