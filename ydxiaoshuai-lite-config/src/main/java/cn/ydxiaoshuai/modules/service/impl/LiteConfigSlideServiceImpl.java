package cn.ydxiaoshuai.modules.service.impl;

import cn.ydxiaoshuai.modules.entity.LiteConfigSlide;
import cn.ydxiaoshuai.modules.mapper.LiteConfigSlideMapper;
import cn.ydxiaoshuai.modules.service.ILiteConfigSlideService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 首页轮播图配置表
 * @Author: 小帅丶
 * @Date:   2020-04-30
 * @Version: V1.0
 */
@Service
public class LiteConfigSlideServiceImpl extends ServiceImpl<LiteConfigSlideMapper, LiteConfigSlide> implements ILiteConfigSlideService {

}
