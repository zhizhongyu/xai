package cn.ydxiaoshuai.modules.controller;

import cn.ydxiaoshuai.common.api.vo.api.*;
import cn.ydxiaoshuai.common.factory.BDFactory;
import cn.ydxiaoshuai.common.sdkpro.AipFacePro;
import cn.ydxiaoshuai.common.system.base.controller.ApiRestController;
import cn.ydxiaoshuai.modules.conts.LogTypeConts;
import cn.ydxiaoshuai.modules.util.ApiBeanUtil;
import cn.ydxiaoshuai.modules.weixin.po.WXAccessToken;
import com.alibaba.fastjson.JSON;
import com.baidu.aip.util.Base64Util;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;




/**
 * @author 小帅丶
 * @className FaceProRestController
 * @Description 人脸邀测接口体验
 * @Date 2020/4/10-14:49
 **/
@Controller
@RequestMapping(value = "/rest/face")
@Scope("prototype")
@Slf4j
@Api(tags = "人脸邀测接口-API")
public class FaceProRestController extends ApiRestController {

    AipFacePro aipFacePro = BDFactory.getAipFacePro();
    @Autowired
    private ApiBeanUtil apiBeanUtil;
    /**
     * @Description 痘斑痣检测接口
     * @param file 图片文件
     * @return void
     * @Author 小帅丶
     * @Date 2020/4/10 14:52
     **/
    @RequestMapping(value = "/detect_acnespotmole", method = {RequestMethod.POST}, produces="application/json;charset=UTF-8")
    public ResponseEntity<Object> detectAcnespotmole(@RequestParam(value = "file") MultipartFile file) {
        log.info("方法路径{},请求IP{}", requestURI,request.getRemoteAddr());
        AcnespotmoleBean bean = new AcnespotmoleBean();
        JSONObject object = null;
        WXAccessToken imgCheckBean = null;
        try {
            startTime = System.currentTimeMillis();
            param = "image="+Base64Util.encode(file.getBytes())+",version="+version+",userId="+userId;
            object = aipFacePro.acnespotmole(Base64Util.encode(file.getBytes()), "BASE64");
            if(LogTypeConts.API_VERSION.equals(version)){
                imgCheckBean = apiBeanUtil.checkImg(request, file);
                if(imgCheckBean.getErrcode()==0){
                    bean = apiBeanUtil.dealFaceAcnespotmoleBean(object,file);
                }else{
                    bean.fail("img fail", imgCheckBean.getErrmsg(),imgCheckBean.getErrcode());
                }
            }else{
                bean = apiBeanUtil.dealFaceAcnespotmoleBean(object,file);
            }
        } catch (Exception e) {
            errorMsg = e.getMessage();
            log.info("痘斑痣检测接口出错了" + errorMsg);
            bean.error("system error", "系统错误");
        }
        //耗时
        timeConsuming = String.valueOf(System.currentTimeMillis() - startTime);
        log.info("耗时{},接口返回内容",timeConsuming,object.toString());
        beanStr = JSON.toJSONString(bean);
        apiBeanUtil.putLog(bean.getLog_id(), timeConsuming, beanStr, ip, param, requestURI, errorMsg, LogTypeConts.FACE_ACNESPOTMOLE,userId,userAgent);
        //响应的内容
        return new ResponseEntity<Object>(JSON.toJSONString(bean), httpHeaders, HttpStatus.OK);
    }

    /**
     * @Description 皮肤光滑度接口
     * @param file 图片文件
     * @return void
     * @Author 小帅丶
     * @Date 2020/4/10 14:52
     **/
    @RequestMapping(value = "/skin_smooth", method = {RequestMethod.POST}, produces="application/json;charset=UTF-8")
    public ResponseEntity<Object> skinSmooth(@RequestParam(value = "file") MultipartFile file) {
        log.info("方法路径{},请求IP{}", requestURI,request.getRemoteAddr());
        SkinSmoothBean bean = new SkinSmoothBean();
        JSONObject object = null;
        WXAccessToken imgCheckBean = null;
        try {
            startTime = System.currentTimeMillis();
            object = aipFacePro.skinSmooth(Base64Util.encode(file.getBytes()), "BASE64");
            param = "image="+Base64Util.encode(file.getBytes())+",version="+version+",userId="+userId;
            if(LogTypeConts.API_VERSION.equals(version)){
                imgCheckBean = apiBeanUtil.checkImg(request, file);
                if(imgCheckBean.getErrcode()==0){
                    bean = apiBeanUtil.dealSkinSmoothBean(object,file);
                }else{
                    bean.fail("img fail", imgCheckBean.getErrmsg(),imgCheckBean.getErrcode());
                }
            }else{
                bean = apiBeanUtil.dealSkinSmoothBean(object,file);
            }
        } catch (Exception e) {
            errorMsg = e.getMessage();
            log.info("皮肤光滑度接口出错了" + errorMsg);
            bean.error("system error", "系统错误");
        }
        //耗时
        timeConsuming = String.valueOf(System.currentTimeMillis() - startTime);
        log.info("耗时{},接口返回内容",timeConsuming,object.toString());
        beanStr = JSON.toJSONString(bean);
        apiBeanUtil.putLog(bean.getLog_id(), timeConsuming, beanStr, ip, param, requestURI, errorMsg, LogTypeConts.FACE_SKIN_SMOOTH,userId,userAgent);
        //响应的内容
        return new ResponseEntity<Object>(JSON.toJSONString(bean), httpHeaders, HttpStatus.OK);
    }

    /**
     * @Description 肤色检测接口
     * @param file 图片文件
     * @return void
     * @Author 小帅丶
     * @Date 2020年4月16日17:42:15
     **/
    @RequestMapping(value = "/skin_color", method = {RequestMethod.POST}, produces="application/json;charset=UTF-8")
    public ResponseEntity<Object> skinColor(@RequestParam(value = "file") MultipartFile file) {
        log.info("方法路径{},请求IP{}", requestURI,request.getRemoteAddr());
        SkinSmoothBean bean = new SkinSmoothBean();
        JSONObject object = null;
        WXAccessToken imgCheckBean = null;
        try {
            startTime = System.currentTimeMillis();
            object = aipFacePro.skinColor(Base64Util.encode(file.getBytes()), "BASE64");
            param = "image="+Base64Util.encode(file.getBytes())+",version="+version+",userId="+userId;
            if(LogTypeConts.API_VERSION.equals(version)){
                imgCheckBean = apiBeanUtil.checkImg(request, file);
                if(imgCheckBean.getErrcode()==0){
                    bean = apiBeanUtil.dealSkinColorBean(object,file);
                }else{
                    bean.fail("img fail", imgCheckBean.getErrmsg(),imgCheckBean.getErrcode());
                }
            }else{
                bean = apiBeanUtil.dealSkinColorBean(object,file);
            }
        } catch (Exception e) {
            errorMsg = e.getMessage();
            log.info("肤色接口出错了" + errorMsg);
            bean.error("system error", "系统错误");
        }
        //耗时
        timeConsuming = String.valueOf(System.currentTimeMillis() - startTime);
        log.info("耗时{},接口返回内容",timeConsuming,object.toString());
        beanStr = JSON.toJSONString(bean);
        apiBeanUtil.putLog(bean.getLog_id(), timeConsuming, beanStr, ip, param, requestURI, errorMsg, LogTypeConts.FACE_SKIN_COLOR,userId,userAgent);
        //响应的内容
        return new ResponseEntity<Object>(JSON.toJSONString(bean), httpHeaders, HttpStatus.OK);
    }
    /**
     * @Description 皱纹检测接口
     * @param file 图片文件
     * @return void
     * @Author 小帅丶
     * @Date 2020年6月17日
     **/
    @RequestMapping(value = "/wrinkle", method = {RequestMethod.POST}, produces="application/json;charset=UTF-8")
    public ResponseEntity<Object> Wrinkle(@RequestParam(value = "file") MultipartFile file) {
        log.info("方法路径{},请求IP{}", requestURI,request.getRemoteAddr());
        SkinSmoothBean bean = new SkinSmoothBean();
        JSONObject object = null;
        WXAccessToken imgCheckBean = null;
        try {
            startTime = System.currentTimeMillis();
            object = aipFacePro.wrinkle(Base64Util.encode(file.getBytes()), "BASE64",1);
            param = "image="+Base64Util.encode(file.getBytes())+",version="+version+",userId="+userId;
            if(LogTypeConts.API_VERSION.equals(version)){
                imgCheckBean = apiBeanUtil.checkImg(request, file);
                if(imgCheckBean.getErrcode()==0){
                    bean = apiBeanUtil.dealWrinkleBean(object,file);
                }else{
                    bean.fail("img fail", imgCheckBean.getErrmsg(),imgCheckBean.getErrcode());
                }
            }else{
                bean = apiBeanUtil.dealWrinkleBean(object,file);
            }
        } catch (Exception e) {
            errorMsg = e.getMessage();
            log.info("皱纹接口出错了" + errorMsg);
            bean.error("system error", "系统错误");
        }
        //耗时
        timeConsuming = String.valueOf(System.currentTimeMillis() - startTime);
        log.info("耗时{},接口返回内容",timeConsuming,object.toString());
        beanStr = JSON.toJSONString(bean);
        apiBeanUtil.putLog(bean.getLog_id(), timeConsuming, beanStr, ip, param, requestURI, errorMsg, LogTypeConts.FACE_WRINKLE,userId,userAgent);
        //响应的内容
        return new ResponseEntity<Object>(JSON.toJSONString(bean), httpHeaders, HttpStatus.OK);
    }

    /**
     * @Description 黑眼圈眼袋检测接口
     * @param file 图片文件
     * @return void
     * @Author 小帅丶
     * @Date 2020年8月26日
     **/
    @RequestMapping(value = "/eyes_attr", method = {RequestMethod.POST}, produces="application/json;charset=UTF-8")
    public ResponseEntity<Object> eyesAttr(@RequestParam(value = "file") MultipartFile file) {
        log.info("方法路径{},请求IP{}", requestURI,request.getRemoteAddr());
        SkinSmoothBean bean = new SkinSmoothBean();
        JSONObject object = null;
        WXAccessToken imgCheckBean = null;
        try {
            startTime = System.currentTimeMillis();
            object = aipFacePro.eyesAttr(Base64Util.encode(file.getBytes()), "BASE64",1);
            param = "image="+Base64Util.encode(file.getBytes())+",version="+version+",userId="+userId;
            if(LogTypeConts.API_VERSION.equals(version)){
                imgCheckBean = apiBeanUtil.checkImg(request, file);
                if(imgCheckBean.getErrcode()==0){
                    bean = apiBeanUtil.dealEyeAttrBean(object,file);
                }else{
                    bean.fail("img fail", imgCheckBean.getErrmsg(),imgCheckBean.getErrcode());
                }
            }else{
                bean = apiBeanUtil.dealEyeAttrBean(object,file);
            }
        } catch (Exception e) {
            errorMsg = e.getMessage();
            log.info("黑眼圈眼袋接口出错了" + errorMsg);
            bean.error("system error", "系统错误");
        }
        //耗时
        timeConsuming = String.valueOf(System.currentTimeMillis() - startTime);
        log.info("耗时{},接口返回内容",timeConsuming,object.toString());
        beanStr = JSON.toJSONString(bean);
        apiBeanUtil.putLog(bean.getLog_id(), timeConsuming, beanStr, ip, param, requestURI, errorMsg, LogTypeConts.FACE_EYES_ATTR,userId,userAgent);
        //响应的内容
        return new ResponseEntity<Object>(JSON.toJSONString(bean), httpHeaders, HttpStatus.OK);
    }

}
